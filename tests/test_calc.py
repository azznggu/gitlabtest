import unittest
import calc

class TestCalc(unittest.TestCase):

    def test_add_three(self):
        self.assertEqual(calc.add(5,3), 8)

    def test_add_three_fail(self):
        self.assertEqual(calc.add(5,3), 9)
